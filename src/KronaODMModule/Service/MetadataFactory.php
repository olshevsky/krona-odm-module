<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:59
 */

namespace KronaODMModule\Service;


use KronaODMModule\AnnotationReader;
use KronaODMModule\ClassMetaData;
use KronaODMModule\Exception\ClassNotFoundException;

class MetadataFactory
{
    protected $documents;

    protected $reader;

    public function __construct()
    {
        $this->reader = new AnnotationReader();
    }

    /**
     * @param  string  $documentClassName
     * @return ClassMetaData
     */
    public function getMetadata($documentClassName)
    {
        if(!isset($this->documents[$documentClassName])){
            $this->fetchMetadata($documentClassName);
        }

        return $this->documents[$documentClassName];
    }

    protected function fetchMetadata($documentClassName)
    {
        if(class_exists($documentClassName)){
            $metadata = new ClassMetaData($documentClassName);
            $this->reader->loadMetadataForClass($documentClassName, $metadata);
            $this->documents[$documentClassName] = $metadata;
        } else {
            throw new ClassNotFoundException();
        }
    }
} 