<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 04:51
 */

namespace KronaODMModule\Service;
use KronaODMModule\DocumentManager;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Factory implements FactoryInterface
{

    protected $instance;

    /**
     * Create DocumentManager
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return DocumentManager
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        if(is_null($this->instance)){
            $this->createInstance($serviceLocator);
        }
        return $this->instance;
    }

    protected function createInstance(ServiceLocatorInterface $serviceLocator)
    {
        $this->instance = new DocumentManager($serviceLocator);
    }
}