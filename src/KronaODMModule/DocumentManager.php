<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 04:38
 */

namespace KronaODMModule;

use KronaODMModule\Document\AbstractDocument;
use KronaODMModule\Repository\AbstractRepository;
use KronaODMModule\Service\MetadataFactory;
use MongoClient;
use MongoDB;
use Zend\ServiceManager\ServiceLocatorInterface;

class DocumentManager
{
    /** @var  MongoClient */
    protected $connection;

    /** @var  MongoDB */
    protected $db;

    /** @var ServiceLocatorInterface  */
    protected $serviceLocator;

    protected $metadataFactory;

    public function __construct(ServiceLocatorInterface $serviceLocator){
        $this->serviceLocator = $serviceLocator;
        $this->metadataFactory = new MetadataFactory();
    }

    /**
     * @param  string  $documentClassName
     * @param  string  $id
     * @return bool|Document\AbstractDocument
     */
    public function find($documentClassName, $id)
    {
        $repository = $this->getRepository($documentClassName);
        return $repository->find($id);
    }

    /**
     * @param  string  $documentClassName
     * @return AbstractRepository
     */
    public function getRepository($documentClassName)
    {
        $metadata = $this->getMetadata($documentClassName);
        $repositoryClassName = $metadata->getRepositoryClassName();
        if(!is_null($repositoryClassName)){
            $repository = new $repositoryClassName($this->getServiceLocator());
        } else {
            $repository = new AbstractRepository($this->getServiceLocator());
        }
        return $repository->setMetadata($metadata);
    }

    /**
     * @param  string  $documentClassName
     * @return ClassMetaData
     */
    public function getMetadata($documentClassName)
    {
        return $this->metadataFactory->getMetadata($documentClassName);
    }

    public function commit(AbstractDocument $document)
    {

    }

    public function getConnection()
    {
        if(is_null($this->connection)){
            $this->connect();
        }
        return $this->connection;
    }

    public function getDB()
    {
        if(is_null($this->connection)){
            $this->connect();
        }
        return $this->db;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    protected function connect()
    {
        $config = $this->serviceLocator->get('Config');
        $mongoConfig = $config['krona']['mongo']['connection']['odm_default']['params'];
        $server = 'mongodb://';
        if(isset($mongoConfig['user']) && $mongoConfig['user'] != ''){
            $server .= $mongoConfig['user'] . ':' . $mongoConfig['password'] . '@';
        }
        $server .=
            (isset($mongoConfig['host'])?$mongoConfig['host']:'localhost') . ':' .
            (isset($mongoConfig['port'])?$mongoConfig['port']:'27017')
        ;
        $server .= '/' . $mongoConfig['dbname'];
        $this->connection = new MongoClient($server);
        $this->db = $this->connection->selectDB($mongoConfig['dbname']);
    }
} 