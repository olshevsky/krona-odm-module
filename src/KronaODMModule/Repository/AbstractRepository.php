<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:32
 */

namespace KronaODMModule\Repository;
use KronaODMModule\ClassMetaData;
use KronaODMModule\Document\AbstractDocument;
use KronaODMModule\DocumentManager;
use KronaODMModule\Exception\EmptyMetadataException;
use MongoCollection;
use Zend\ServiceManager\ServiceLocatorInterface;

class AbstractRepository
{
    /** @var ServiceLocatorInterface  */
    protected $sm;

    /** @var DocumentManager  */
    protected $dm;

    /** @var  ClassMetaData */
    protected $metadata;

    /** @var  MongoCollection */
    protected $collection;

    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->sm = $serviceLocator;
        $this->dm = $serviceLocator->get('krona.documentmanager');
    }

    /**
     * @param ClassMetaData $metadata
     * @return self
     */
    public function setMetadata(ClassMetaData $metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * @return AbstractDocument
     */
    public function createDocument()
    {
        $className = $this->getMetadata()->getDocumentClassName();
        return new $className($this->getMetadata());

    }

    /**
     * @return ClassMetaData
     * @throws \KronaODMModule\Exception\EmptyMetadataException
     */
    public function getMetadata()
    {
        if(is_null($this->metadata)){
            throw new EmptyMetadataException();
        }

        return $this->metadata;
    }

    public function getCollection()
    {
        if(is_null($this->collection)){
            $this->createCollection();
        }

        return $this->collection;
    }

    public function find($id)
    {
        $data = $this->getCollection()->findOne(array(
            '_id' => $id
        ));
        if(is_null($data)){
            return false;
        }
        $doc = $this->createDocument();
        $doc->setFromArray($data);
        return $doc;
    }

    public function findOneBy(array $criteria)
    {
        $data = $this->getCollection()->findOne($criteria);
        if(is_null($data)){
            return false;
        }
        $doc = $this->createDocument();
        $doc->setFromArray($data);
        return $doc;
    }

    protected function createCollection()
    {
        $this->collection = $this->dm->getDB()->createCollection($this->getMetadata()->getCollectionName());
    }
} 