<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:00
 */

namespace KronaODMModule\Mapping;

/**
 * Class Document
 * @package KronaODMModule\Mapping
 * @Annotation
 * @Target("CLASS")
 * @Required
 * @Attributes(
 *  @Attribute("repositoryClass", type="string"),
 *  @Attribute("collectionName", type="string", required=true)
 * )
 */
class Document
{
    public $repositoryClass;

    public $collectionName;
}