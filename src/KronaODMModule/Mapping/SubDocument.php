<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:09
 */

namespace KronaODMModule\Mapping;

/**
 * Class SubDocument
 * @package KronaODMModule\Mapping
 * @Annotation
 * @Target("CLASS")
 * @Required
 */
class SubDocument
{
//    public $targetEntity;
} 