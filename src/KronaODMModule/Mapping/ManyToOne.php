<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:13
 */

namespace KronaODMModule\Mapping;

/**
 * Class ManyToOne
 * @package KronaODMModule\Mapping
 * @Annotation
 * @Target("PROPERTY")
 */
class ManyToOne
{
    public $targetEntity;
} 