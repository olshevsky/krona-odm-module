<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:15
 */

namespace KronaODMModule\Mapping;

/**
 * Class Field
 * @package KronaODMModule\Mapping
 * @Annotation
 * @Target("PROPERTY")
 */
class Field
{
    public $name;

    public $type;
} 