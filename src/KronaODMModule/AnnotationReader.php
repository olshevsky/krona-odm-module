<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 08:13
 */

namespace KronaODMModule;
use Doctrine\Common\Annotations\AnnotationReader as DoctrineAnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use KronaODMModule\Mapping\Document;
use KronaODMModule\Mapping\Field;
use KronaODMModule\Mapping\ManyToOne;
use KronaODMModule\Mapping\SubDocument;
use ReflectionClass;
use ReflectionProperty;

class AnnotationReader
{
    protected $reader;

    public function __construct()
    {
        AnnotationRegistry::registerAutoloadNamespace('KronaODMModule\Mapping', array(realpath(__DIR__ . '/../')));
        $this->reader = new DoctrineAnnotationReader();
    }

    public function loadMetadataForClass($className, ClassMetaData $metadata)
    {
        $class = new ReflectionClass($className);
        $this->parseClassAnnotations($this->reader->getClassAnnotations($class), $metadata);
        foreach($class->getProperties() as $reflection){
            $this->parsePropertyAnnotations($this->reader->getPropertyAnnotations($reflection), $metadata, $reflection);
        }
    }

    protected function parseClassAnnotations($annotations, ClassMetadata $metadata)
    {
        foreach($annotations as $annotation)
        {
            if($annotation instanceof Document){
                $metadata->setRepositoryClassName($annotation->repositoryClass);
                $metadata->setCollectionName($annotation->collectionName);
            } elseif($annotation instanceof SubDocument){
                $metadata->setIsSubclass(true);
            }
        }
    }

    protected function parsePropertyAnnotations($annotations, ClassMetaData $metadata, ReflectionProperty $reflection)
    {
        $mapping = array();
        foreach($annotations as $annotation){
            if($annotation instanceof Field){
                $mapping['field'] = array(
                    'name' => $annotation->name,
                    'type' => $annotation->type
                );
            } elseif($annotation instanceof ManyToOne) {
                $mapping['mapping'] = array(
                    'targetEntity' => $annotation->targetEntity
                );
            }
        }
        isset($mapping['field']) && $metadata->addField($mapping['field']['name'], $reflection->getName());
    }
} 