<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:17
 */

namespace KronaODMModule\Document;
use KronaODMModule\ClassMetaData;

class AbstractDocument
{
    protected $data = array();

    /** @var \KronaODMModule\ClassMetaData */
    protected $metadata;

    public function __construct(ClassMetaData $metadata)
    {
        $this->metadata = $metadata;
    }

    public function get($name)
    {
        $fieldName = $this->metadata->getFieldName($name);
        if($fieldName){
            return $this->$fieldName;
        } else {
            return (isset($this->data[$name])?$this->data[$name]:false);
        }
    }

    public function set($name, $value)
    {
        $fieldName = $this->metadata->getFieldName($name);
        if($fieldName){
            $this->data[$name] = $value;
            $this->$fieldName = $value;
        } else {
            $this->data[$name] = $value;
        }
        return $this;
    }

    public function getCleanData()
    {
        $result = array();
        foreach($this->data as $column => $value)
        {
            $fieldName = $this->metadata->getFieldName($column);
            if($fieldName){
                if($this->metadata->hasMapping($fieldName)){
                    //TODO: Implement mappings
                } else {
                    $result[$column] = $this->$fieldName;
                }
            } else {
                $result[$column] = $value;
            }
        }
        return $result;
    }

    public function setFromArray(array $data)
    {
        foreach($data as $key => $value){
            if(!$this->metadata->hasMapping($this->metadata->getFieldName($key))){
                $this->set($key, $value);
            } else {
                //TODO: Implement Init SubDocuments
            }
        }
    }
} 