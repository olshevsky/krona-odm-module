<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 07:17
 */

namespace KronaODMModule;


class ClassMetaData
{
    protected $documentClassName;

    protected $collectionName;

    protected $repositoryClassName;

    protected $fieldsNames;

    protected $fieldsMappings;

    protected $isSubclass = false;

    public function __construct($documentName)
    {
        $this->documentClassName = $documentName;
    }

    public function getFieldName($columnName)
    {
        return (isset($this->fieldsNames[$columnName])?$this->fieldsNames[$columnName]:false);
    }

    public function getColumnName($fieldName)
    {

    }

    public function hasMapping($fieldName)
    {
        return isset($this->fieldsMappings[$fieldName]);
    }

    public function getDocumentClassName()
    {
        return $this->documentClassName;
    }

    public function getCollectionName()
    {
        return $this->collectionName;
    }

    public function getRepositoryClassName()
    {
        return $this->repositoryClassName;
    }

    /**
     * @param mixed $collectionName
     */
    public function setCollectionName($collectionName)
    {
        $this->collectionName = $collectionName;
    }

    /**
     * @param mixed $documentClassName
     */
    public function setDocumentClassName($documentClassName)
    {
        $this->documentClassName = $documentClassName;
    }

    /**
     * @param mixed $repositoryClassName
     */
    public function setRepositoryClassName($repositoryClassName)
    {
        $this->repositoryClassName = $repositoryClassName;
    }


    /**
     * @param boolean $isSubclass
     */
    public function setIsSubclass($isSubclass)
    {
        $this->isSubclass = $isSubclass;
    }

    /**
     * @return boolean
     */
    public function getIsSubclass()
    {
        return $this->isSubclass;
    }

    public function addField($field, $column)
    {
        $this->fieldsNames[$field] = $column;
    }


} 