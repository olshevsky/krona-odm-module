<?php
/**
 * Created by PhpStorm.
 * User: krona
 * Date: 19.10.13
 * Time: 04:49
 */
return array(
    'factories' => array(
        'krona.documentmanager' => new \KronaODMModule\Service\Factory()
    )
);